프로젝트 구현때 도움이 될만한 것이나 유용한 클래스들을 따로 모듈화 시켜 재사용이 가능하도록 한 프로젝트 입니다.

사용법은 우선

1. 프로젝트 폴더의 [Source]-[SSH_helperModule] 폴더에 해당 모듈을 넣어준다.
  1-1. git을 사용하지 않는경우...

    1-1-1. 프프로젝트 압축파일을 받아서 위 폴더에 풀어주고 사용
    
    1-1-2. 이때 폴더명은 반드시 SSH_helperModule 이어야하고 위 폴더가 모듈의 루트폴더.
    
           잘못풀어서 [Source]-[SSH_helperModule]-[SSH_helperModule] 이런식으로 풀리지 않도록 주의!!!
           
           
  1-2. 메인 프로젝트를 git으로 관리하는 경우...
  
    1-2-1. 메인프로젝트역시 git으로 관리하는 경우 위 모듈은 서브모듈로 관리해야한다.
    
    1-2-2. 프로젝트의 루트에서...
    
            git submodule add https://gitlab.com/UE4_Plugin/modules/ssh_helpermodule.git ./Source/SSH_helperModule
            
            명령으로 위 프로젝트를 서브모듈로 관리
            
    1-2-3. 가급적인 서브 모듈은 클론뜨자마자 바로 브랜치따서 작업한후 작업이 끝나면 master 브랜치와 머지해서 푸시하자        
    
    
            
  1-3. 모듈만 git으로 관리하고 싶은경우...
  
    1-3-1. 프로젝트에 [Source]-[SSH_helperModule] 폴더생성
    
    1-3-2. 해당 폴더로 이동후
    
           git clone https://gitlab.com/UE4_Plugin/modules/ssh_helpermodule.git ./
           
           명령으로 클론뜨고 사용
           
           
2. 모듈을 넣어줬으면 .uproject파일과 프로젝트의 .Target.cs 파일을 수정해서 모듈을 사용할수 있도록 세팅


project$1709679

![Alt text](https://gitlab.com/snippets/1709679)

https://gitlab.com/snippets/1709679

3. 그후 프로젝트 파일 다시 생성후 빌드